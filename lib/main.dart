import 'dart:math';

import 'package:flutter/material.dart';

void main() => runApp(
      MaterialApp(
        home: Scaffold(
          backgroundColor: Colors.lightBlue,
          appBar: AppBar(
            title: Text('Ask me anything'),
            backgroundColor: Colors.blue,
          ),
          body: MagicBallPage(),
        ),
      ),
    );

class MagicBallPage extends StatefulWidget {
  @override
  _MagicBallPageState createState() => _MagicBallPageState();
}

class _MagicBallPageState extends State<MagicBallPage> {
  int ballNumber = 1;
  @override
  Widget build(BuildContext context) {
    return Center(
      child: FlatButton(
        onPressed: () {
          changeState();
        },
        child: Image.asset('images/ball$ballNumber.png'),
      ),
    );
  }

  void changeState() {
    setState(() {
      ballNumber = generateNewNumbers();
    });
  }

  int generateNewNumbers() {
    int ballNumber = Random().nextInt(5) + 1;
    if (ballNumber > 6) ballNumber = 1;
    return ballNumber;
  }
}

